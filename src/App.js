/* eslint-disable jsx-quotes */
/* eslint-disable react-native/no-inline-styles */
/* eslint-disable prettier/prettier */
// import 'react-native-gesture-handler';
import React from 'react';
import {NavigationContainer} from '@react-navigation/native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {createMaterialBottomTabNavigator} from '@react-navigation/material-bottom-tabs';
import {createMaterialTopTabNavigator} from '@react-navigation/material-top-tabs';
import {createDrawerNavigator} from '@react-navigation/drawer';
import ScreenA from './Screens/ScreenA';
import ScreenB from './Screens/ScreenB';
import ScreenC from './Screens/ScreenC';

// import FontAwesome5 from 'react-native-vector-icons/FontAwesome5';
import Headers from './Headers';

// const Tab = createBottomTabNavigator();
// const Tab = createMaterialBottomTabNavigator();
const Drawer = createDrawerNavigator();

function App() {
  return (
    <>
      <NavigationContainer>
        <Drawer.Navigator
          initialRouteName="ScreenA"
          drawerPosition="right"
          drawerType="back"
          // drawerStyle={{
          //     backgroundColor:"grey"
          //    }}
          screenOptions={{
            headerShown: true,
            swipeEnabled: false,
            headerTitleAlign: 'center',
            // headerStyle: {
            //   backgroundColor: 'black',
            // },
            headerTintColor: 'black',
            headerTitleStyle: {
              fontSize: 20,
            },
          }}>
          <Drawer.Screen
            name="Demo"
            component={ScreenA}
            options={{
              title: 'Demo',
              drawerStyle:{
              backgroundColor:"#4E9D7d",
              color:"white",
             }
            }}
          />
          <Drawer.Screen
            name="Home"
            component={ScreenA}
            options={{
              title: 'Home',
             drawerStyle:{
              backgroundColor:"darkgrey"
             }
            }}
          />
          <Drawer.Screen
            name="Create MRR"
            component={ScreenB}
            options={{
              title: 'Create MRR',
            }}
          />
          <Drawer.Screen
            name="View MRR"
            component={ScreenC}
            options={{
              title: 'View MRR',
            }}
          />
          <Drawer.Screen
            name="Create Sale Order"
            component={ScreenC}
            options={{
              title: 'Create Sale Order',
            }}
          />
          <Drawer.Screen
            name="Bussiness Profile"
            component={ScreenC}
            options={{
              title: 'Bussiness Profile',
            }}
          />
          <Drawer.Screen
            name="Change Language"
            component={ScreenC}
            options={{
              title: 'Change Language',
            }}
          />
          <Drawer.Screen
            name="Call Support"
            component={ScreenC}
            options={{
              title: 'Call Support',
            }}
          />
          <Drawer.Screen
            name="Logout"
            component={ScreenC}
            options={{
              title: 'Logout',
            }}
          />
        </Drawer.Navigator>
      </NavigationContainer>
    </>
  );
}

export default App;
