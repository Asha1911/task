/* eslint-disable prettier/prettier */

import React, {useEffect, useState} from 'react';
import {
  StyleSheet,
  View,
  Text,
  Pressable,
  Modal,
  Button,
  TouchableOpacity,
  BackHandler,
  Image,
} from 'react-native';
import {ScrollView} from 'react-native-gesture-handler';
import Material from './Material';
import Saleorder from './Saleorder';
import Jobs from './Jobs';
import StockManger from './StockManger';
import Check from './Check';
import {SafeAreaView} from 'react-native-safe-area-context';
import {Checkbox} from 'react-native-paper';

const Modalpopup = ({visible, children}) => {
  const [showModal, setShowModal] = useState(visible);

  useEffect(() => {
    toggleModal();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [visible]);

  const toggleModal = () => {
    if (visible) {
      setShowModal(true);
    } else {
      setShowModal(false);
    }
  };
  

  return (
    <ScrollView>
      <Modal transparent visible={showModal}>
        <View style={styles.modalbackground}>
          <View style={[styles.modalcontainer]}>{children}</View>
        </View>
      </Modal>
    </ScrollView>
  );
};


function ScreenA({navigation}) {
  const [visible, setVisible] = useState(false);
  const [posts, setPosts] = useState([]);
  useEffect(() => {
    fetch('https://service.staging.recykal.com/core/locations?page=0&size=20', {
      method: 'get',
      headers: {
        Authorization:
          'Bearer eyJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxNDU4MTUiLCJhdXRoIjoiUk9MRV9GUkFOQ0hJU0VfQURNSU4sUk9MRV9GUkFOQ0hJU0VfVVNFUiIsInVzZXIiOnsidXNlcklkIjoxNDU4MTUsImZyYW5jaGlzZUlkIjo2LCJsb2dpbiI6IjE0NTgxNSIsImVtYWlsIjoiZGVtb3BhcnRuZXJAcG9rZW1haWwubmV0IiwiZXByVXNlckN1c3RvbWVySWQiOjMwNywidmVyc2lvbiI6IjEuMC4wIn0sImV4cCI6MTYzNjI2OTMyOX0.jI_MWq1I_u_Gl0TZSzoj57nbenof-05v-2ud7eN7ALNylKXyPK_8sFDYBpTchq3fQffM8GQentPesOtKE20TbQ',
        'Content-Type': 'application/json;charset=UTF-8',
      },
    })
      .then(res => res.json())
      .then(json => setPosts(json));
  }, []);

  // const handleName =() =>{
  //   alert("selected")
  // }
  return (
    <>
      <ScrollView>
        <View style={styles.body}>
          <Text style={styles.text}>Welcome, Demo Partner</Text>
          <Text style={styles.background}>.</Text>
          <Text style={styles.demo_text}>... Demo Partner</Text>
          <Modalpopup visible={visible}>
            <ScrollView>
              <View style={{alignItems: 'center'}}>
                <View style={styles.header}>
                  <Text style={styles.heading}>
                    Select the center for which you have to maintain the order
                  </Text>
                  {posts.map(post => (
                    <>
                      <View style={styles.item}>
                        <Pressable
                          style={({pressed}) => [
                            {backgroundColor: pressed ? '#27353C' : 0},
                            styles.btn,  
                          ]}>
                          {/* <Image
                            style={styles.logo}
                            source={{
                              uri: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAATYAAACjCAMAAAA3vsLfAAAAbFBMVEX///8AAADm5ubl5eXk5OTx8fH4+Pjt7e3z8/P6+vrq6ur8/Pzu7u7a2try8vIcHBzDw8NSUlJnZ2ecnJynp6e7u7vKysqzs7Otra0hISEwMDBPT0+ioqJsbGwJCQkTExOAgIBdXV2SkpJCQkIedoNQAAAJ5UlEQVR4nO2d63qcOAyGKeYMpWm7aZMettnd+7/HRdjIBoSBGRvkCfr1PfGEUd74JGNbUQQWd5b0qgJVg8pFHIsMVAOqANWCSkEVoBpQZafiKjLs4wc3Fjm3Av3PQOW9/8ngfw0q7j+YIJAYgQAaUUo00YXtwnZhCwFbAiaxgapRSWygJDZQ8mtBSWygQsGGXvf+5xIW+q8xaBWP0UhsiVHRKK4NVrkUqxz5LxPyJ7yxpdrrSUOpscr1FU3WJjFqg2LUBklsXVmyD1siHQkMW4dIUNgSAluyju2W2hYAtmZrbTsQ2yM10nVsk85Pjwaq8xvGhRTHhWI8Lgz9agDYmonXejTrxwU1iC6NCyM0dWdR1VlcR6DiTpWgclAZqBRUAaoF1YIqQDWgMlA5KAJb/y1jq/7B0n/jefE3j9ik1+nY6xJU1XOIgQOoCFQcEWh6II0EbFZHYVbHZHHeRvWrBDbK9y9Y+pUojb1i0/4nQ/9Sa/9X5m04UHqf7lK+27GJg7CxjhIo33nUNtZRAuX7mbVtPUoQqKxRAnR5FfSHda9KUDmoDFQDqgCVoipApaAyUI1SBTrHtra1KfrfoP85qHKCYaJyRNNINOa8TdBRQrI8b0vilSiBcv682tZor4koQUzbp22g9BslUM6fh83hdNdvlEA5f3ptc4FNdDZ0fp1So0GnVOfXKRUldEpFCZ1S1b1TQ78aQG1rtNeqkXZKYeuUaqQIJEkGVc3QlJ2p0QCUHA1AydEAfpii6nvTFlUKSo0LJRklUM6fh63WXqtxYfA/B1WiWgbSq1RNHIRYixKSaZRA9KuDscXWNZTB66UoYTYuUAMlw+nuFSU8PDbdDU46PxUlDKNBOx4NJv3qYIyxodfmaIb+AwaByjZQRjlY1lnZjFUBKh1U1qIqQKXlSGVdGTrHFluREv43yn9T5aBKCo0CYk5ABHIVyFXgBESYExCBVS7EKEEIgQ1F4ARETCcgGk01RnNFCVeUsG5+ogRBdX7uo4TfWPqLKK0OaaRUlDBrpLaBEjq6rAHrVQqqGKsWVQGqRZWaqkTvDGwvf83s5T8s/U0Uf/WIrbT5P8JAKRONfB7V+enRoMHRYDwBmfarBLa7zDk2OZrhBETgBERYJiDkQCkf5226yw1bIFHCY2MjG6mLKIEbtoUoYVsjNaOEFKwAo1RLqL64namMP7Zs7rWpljHMgGyPEhYmIMI6AeGFjZqAnB0lBLAHJJgogRc231GCbqR3RgkssW1tpImlkRaOrCWHhM+EvWLpD6L0p0dsakhwYPJ5905Adr1LsMekjUdsd05AjDYoH3ctioPiEyVQvj8OtpVQ3m0jZfMu4Z5Qvu/hdgUIWo2m2uTCEeX7edhKFwFCr05Ypjx9dfeGZcoxmmtR/PQoIYDa5jC4chPKv7sXfvbXCDteKITwennxNcLKC4UZEOUr8Y7GyTIl5TybCci2zQz0MqUsu6a70RUlLNqxUcLtL/wo309upG5e+GULe2syvdkoHW82avW2HVMFsQl15vWw2QhVrrcdTYCYaNhtefZ5nGPvlmfLZoYrSgAVfJTgs7adESXAB69tgQMQ6Of6HfhlXnUK9t2XDaoUVH8koUVVgEpRNepwwuYowb4onnrEVqRj/zPD/07lPYYKgMjDCcto5PMWz3vcGyW8fprZ699Y+oco/mT95Rvsw0eD22qUkJBbnjWaY6IEDmZiCyRK4GD+sBljhth4xcC2w0McbIrNyeEh+8HlbHxwue2PqhFHmOEIcAjY6IPXq0eYZ2jcXZ/ifjODI9PYHF6fwnjrjCObYeMfJXCw47Bh57eCbfWKAQ5mwTa+iGzPFQPyDh99mw8o6PyG23zmFx2NLwqq4KKjuozjlj+2NJ1fz1Tl0v/heqN6er0RqtJAI83b9Snfnmb27TOWvhHF36m//DvxmDcs/Ux9C4XN4fUpun0utGKPi+Irh4e0VcQH9XGZL9S30NgCiRLs2FZWQLSJB8Tm7YqBY7BNBtG9F5EJxKbu/IzXsMXN4ICfG7V8Y4vV3Alrm1jAJqa1LfZ8yeJ+bD5rG8cowf2iuHNsHM8luF8U91XbHhFbILXt6tuU//v6tskHOF2y6BzbLVexC35RAot522NECQfO2x4JW0i1jVHf5re2Oevb7lqmNP5lHrbOOMdmz5cgLBMQsWnedgu2EOdtXNNM7Mf23qKEEBtpmFHC0au7rqOEOO5zKfRc+zQTQCweXjaoty+dqTQTUTS8xwAvbGkmuL1LkG+PtNfATqaZAHaoVJoJBDJB4z3NBAej522s00xwsACjBA4WYJTAwby8J/WbZoKDaWw3p5nQaI5JM8HB5hOQheCKT5oJDsb7Fcz72t/m98wVB5s30rUzVxglLBw7WE0zobIqqDQTsjdta0wzgWkaltJM3HUuQRvxsdc/WPo39S0Utlp7vZhmouKaZmLz1VDaGuKDv7D0N/UtFLb3kWZCG9NF8QvbTdhsB07Pu5nBA7YbbmYgB8rlNBNGVoWyUy0qTNNQek4z4RxbMfH65jQTJec0E86xvY80E86xBRMl8MIWdJqJ07HdcH/bbKD0m2aCF7Zg0kzwwhZMmglu2K4ogQE2spG+oyiBW5oJXtjINBNb7o49Os0EL2wco4QAprtXlMAyStCN9OGjhL1pJhQQ2dGBTVSxU20eEs5bFM8W/7rpH78GRD5vJUpYnYAsRgl3ZefQ9pP44A8sfaW+hcIWTJoJDhZglMDBDo4SXDRSDrbYSB2mmaDCgjvSTHAwY+GoXUozsSVoODDNBAdzsUw5m4BcW2cktrOihABqWzBpJjiYl1Deb5oJDmZGCYGkmdD2a57U28j+/Ub9CmVvRHJwbb+oX7lzMwO9TCnL/E93s8hmT1uxPVkfk61hCy5KaCObPW/F9mx9THssNm8v/Lhhc/bCDzu/IZfCoFJQxVgVoFpUqalWLpDlgK2eew1bjDrVDKrRKqfQFIekmeCFLZg0EyyxhRQlcMB2UpqJ/dsCeWHzk2Yix1wKuas0E7ywUWkmckwzkRtpJjQQGo18nrc0E7ywBZhmgge2K0pggG398NDdaSb4YGOUZqJZTTPBAdvSwWvGaSY4YON4gewVJVxRwm3YsPNzl2aCAzZ7moldVwwsp5kABeNCDKNBlFZmmokKEjbUozQTK1GCLvaIrbBj02kmpNc3p5lQw5+L61P6kaK/gIRc4f6YWCx+2YrtJbY9h/yHPYmhuNBes0kzQV6FxcueS35RwuZ2dp6dj23er4aK7d40E1OkMWKLTWwx1rZxmolQsMWIzZJmApRYRNO4SzNRBdC3fS/GXjOIEqoAapvCxilKCKG2PbPDlryz2nb1bRv6topAY/vAjighgNr2XK5HCVNi9FXs/wPWPcjVzrP8PAAAAABJRU5ErkJggg==',
                            }}
                          /> */}
                          <Text style={styles.titles}>{post.name}</Text>
                          <Text style={styles.title}>
                            {post?.addressInfo?.street}
                          </Text>
                        </Pressable>
                      </View>
                    </>
                  ))}
                </View>
              </View>
            </ScrollView>
            <Check />
          </Modalpopup>
          <Text style={styles.icon} onPress={() => setVisible(true)}>
            /
          </Text>
        </View>
        <Material />
        <Saleorder />
        <Jobs />
        <StockManger />
      </ScrollView>
    </>
  );
}
const styles = StyleSheet.create({
  body: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: 20,
    fontWeight: 'bold',
    color: '#4E9D7d',
    marginBottom: 12,
  },
  background: {
    width: 375,
    height: 50,
    backgroundColor: '#adadc9',
    borderRadius: 8,
    opacity: 0.3,
    color: 'black',
    textAlign: 'left',
  },
  logo: {
    width: 50,
    height: 30,
    bottom: 165,
    left: 130,
  },
  demo_text: {
    color: 'black',
    textAlign: 'left',
    bottom: 35,
    marginRight: 250,
    fontSize: 15,
    fontWeight: 'bold',
  },
  icon: {
    backgroundColor: '#696880',
    borderRadius: 40,
    width: 40,
    height: 40,
    bottom: 65,
    left: 150,
    textAlign: 'center',
    fontWeight: 'bold',
  },
  modalbackground: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: 'rgba(0,0,0,0.5)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalcontainer: {
    width: '99%',
    flex: 1,
    backgroundColor: 'white',
    paddingHorizontal: 20,
    paddingVertical: 30,
    borderRadius: 8,
    elevation: 20,
  },
  btn: {
    borderRadius: 8,
    // width: 375,
    // height: 50,
   paddingHorizontal: 10,
  },
  item: {
    backgroundColor: '#E5ECEF',
    padding: 20,
    borderRadius: 8,
    marginTop: 15,
  },
  title: {
    fontSize: 15,
    fontWeight: '700',
    bottom: 10,
    textAlign: 'center',
    marginTop: 10,
    color: 'grey',
  },
  titles: {
    fontSize: 15,
    fontWeight: '700',
    bottom: 10,
    textAlign: 'center',
    marginTop: 10,
    color: 'green',
  },
  press_view: {
    backgroundColor: '#4E9D7d',
    height: 40,
    width: '100%',
    borderRadius: 8,
    justifyContent: 'center',
  },
  press_btn: {
    backgroundColor: '#4E9D7d',
    height: 40,
    width: '100%',
    borderRadius: 8,
    justifyContent: 'center',
  },
  heading: {
    fontWeight: 'bold',
    fontSize: 15,
    textAlign: 'center',
  },
  // header:{

  // }
});
export default ScreenA;
