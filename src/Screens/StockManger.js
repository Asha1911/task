/* eslint-disable prettier/prettier */

import React from 'react';
import {StyleSheet, View, Text, Pressable} from 'react-native';

export default function ScreenB({navigation}) {
  const onPressHandler = () => {
    // navigation.navigate('Screen_A');
    navigation.goBack();
    // navigation.openDrawer();
    // navigation.setParams({ItemId:14})
  };

  return (
    <>
      <View style={styles.body}>
        <Text style={styles.stockmanager}>
          {'\n'}
          {'\n'}
          {'\n'}
          {'\n'}
          Stock Transfer
          {'\n'}
          {'\n'}
          View
        </Text>
        <Text style={styles.stocktransfer}>
          {'\n'}
          {'\n'}
          {'\n'}
          {'\n'}
          Stock Transfer
          {'\n'}
          {'\n'}
          View
        </Text>
      </View>
    </>
  );
}
const styles = StyleSheet.create({
  body: {
    justifyContent: 'center',
    alignItems: 'center',
    bottom: 365,
    flex: 1,
    flexDirection: 'row',
  },
  text: {
    fontSize: 20,
    fontWeight: 'bold',
    color: 'white',
    margin: 5,
  },
  stockmanager: {
    width: 185,
    height: 150,
    backgroundColor: '#adadc9',
    borderRadius: 8,
    opacity: 0.3,
    color: 'black',
    textAlign: 'center',
    fontWeight: 'bold',
  },
  stocktransfer: {
    width: 185,
    height: 150,
    backgroundColor: '#adadc9',
    borderRadius: 8,
    opacity: 0.3,
    color: 'black',
    textAlign: 'center',
    marginLeft: 8,
    fontWeight: 'bold',
  },
});
