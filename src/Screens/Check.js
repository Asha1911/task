/* eslint-disable prettier/prettier */

import React from 'react';
import {StyleSheet, View, Text, Pressable, Image} from 'react-native';

export default function Check({navigation}) {
const handlePress = () =>{
alert("successfully selected")
}
  return (
    <Pressable style={styles.press_view} onPress={()=>handlePress()}>
      <Text style={styles.text}>Proceed</Text>
    </Pressable>
  );
}
const styles = StyleSheet.create({
 
  text: {
    fontSize: 15,
    fontWeight: 'bold',
    color: 'white',
    margin: 12,
    textAlign:"center",
    justifyContent:"center",

  },
  press_view: {
    backgroundColor: '#328E69',
    height: 50,
    width: '100%',
    borderRadius:5,
    marginTop:8

  },
 
 
});
