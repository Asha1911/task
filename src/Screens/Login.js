/* eslint-disable prettier/prettier */

import React from 'react';
import {StyleSheet, View, Text, Pressable} from 'react-native';

export default function Login({navigation}) {


  return (
    <View style={styles.body}>
      <Text>Login</Text>
    </View>
  );
}
const styles = StyleSheet.create({
  body: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {
    fontSize: 40,
    fontWeight: 'bold',
    margin: 10,
  },
});
